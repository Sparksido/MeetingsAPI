﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Web.UI;
using System.Configuration;
using MeetingsAPI.Controllers;
using ClassLibrary1;

namespace MeetingsAPI
{
    public class EmailJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                MeetingController appoint = new MeetingController();
		
		var item_string = "";
                foreach (Appointment schedule_item in appoint.Get())
                {
                    var item_number = schedule_item.PhoneNumber;
                    var item_location = schedule_item.Location;
                    item_string = item_string + System.Environment.NewLine + "a client of Phone Number: " item_number + " at " + item_location + " and   ";
                }

                DateTime today = DateTime.Today;
                mailMessage.From = new MailAddress(ConfigurationManager.AppSettings["FromMail"]);
                mailMessage.Subject = "Api Schedule Tester";
                mailMessage.Body = "You have meetings with  " + item_string;
                //mailMessage.Body = (appoint.Get(today)).Content.ReadAsStringAsync();
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress("mopatib@gmail.com"));
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["Host"];
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["FromMail"];
                NetworkCred.Password = ConfigurationManager.AppSettings["Password"];
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                smtp.EnableSsl = true;
                smtp.Send(mailMessage);
            }
        }
    }
}