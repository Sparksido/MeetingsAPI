﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using System.IO;

namespace MeetingsAPI
{
    public class JobScheduler
    {
        public static void Start()
        {
            IJobDetail job = JobBuilder.Create<EmailJob>()
            .WithIdentity("job1")
            .Build();

            ITrigger trigger = TriggerBuilder.Create()
            .WithDailyTimeIntervalSchedule
            (s =>
                         s.WithIntervalInSeconds(24)
                        .OnEveryDay()
            )
                        .ForJob(job)
                        .WithIdentity("trigger1")
                        .StartNow()
                        .WithCronSchedule("0 0/1 * * * ?")
                        .Build();

            ISchedulerFactory sf = new StdSchedulerFactory();
            IScheduler sc = sf.GetScheduler();
            sc.ScheduleJob(job, trigger);
            sc.Start();
        }
    }
}