﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClassLibrary1;

namespace MeetingsAPI.Controllers
{
    public class MeetingController : ApiController
    {
        //GET api/appointments/
        public IEnumerable<Appointment> Get()
        {
            using (AppointmentsDataEntities entities = new AppointmentsDataEntities())
            {
                return entities.Appointments.ToList();
            }
        }

        //GET api/appointments/id
        public HttpResponseMessage Get(int id)
        {
            using (AppointmentsDataEntities entities = new AppointmentsDataEntities())
            {
                var meeting = entities.Appointments.FirstOrDefault(appo => appo.Id == id);

                if(meeting != null)
                {
                    var message = Request.CreateResponse(HttpStatusCode.OK, meeting);
                    return message;
                }
                else
                {
                    var message = Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                                            "There are no appointments with id " + id);
                    return message;
                }
            }
        }

        //POST api/appointments/  body={"Name":"name", "PhoneNumber":"phone_number","Location":"location", "Day":"day"}
        public HttpResponseMessage Post([FromBody] Appointment appo)
        {
            try
            {
                using (AppointmentsDataEntities entities = new AppointmentsDataEntities())
                {
                    entities.Appointments.Add(appo);
                    entities.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, appo);
                    message.Headers.Location = new Uri(Request.RequestUri + appo.Day.ToString());
                    return message;
                }
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
